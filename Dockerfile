FROM pull-through.cloud.duke.edu/library/caddy:builder as caddy-build

RUN xcaddy build --with github.com/corazawaf/coraza-caddy/v2

FROM pull-through.cloud.duke.edu/library/caddy:2.7.6

COPY --from=caddy-build /usr/bin/caddy /usr/bin/caddy